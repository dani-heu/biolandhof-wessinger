<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//DE" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="de">
<head profile="http://gmpg.org/xfn/11">

   <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

   <title><?php wp_title(' - ', true, 'right'); ?> <?php bloginfo('name'); ?></title>

   	<!-- Bootstrap CSS -->
	<link href="<?php echo get_template_directory_uri() ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

   <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
   <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

   <?php wp_head(); ?>

</head>
<body>

<div class="container">
   <header class="row">
		<div class="col-md-3 col-6">
			<a href="<?php bloginfo('url'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" /></a>
		</div>
		<div  class="col-md-6 col-sm-6  d-none d-sm-block text-center title" >
			<h2>BiolandHof</h2>
			<h1>Wessinger</h1>
		</div>
		<div class="col-md-3 col-6 d-none d-md-block">
			<img id="schriftzug" src="<?php echo get_template_directory_uri(); ?>/assets/images/schrfitzug.png">
		</div>

	</header>
	<div class="ketten_wrapper">
		<div class="kette_links"></div>
		<div class="kette_rechts"></div>
	</div>
	<nav>
		<?php wp_nav_menu('primary'); ?>
	</nav>

