<?php get_header(); ?>

<div id="main">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="row">
			<h3 class="col"><?php the_title(); ?></h3>
		</div>
		<div class="row">
			<div class="entry col">
				<?php the_content(); ?>
			</div>
		</div>
	<?php endwhile; endif; ?>

</div><!-- main -->

<?php get_footer(); ?>